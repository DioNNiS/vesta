# frozen_string_literal: true

module Vesta
  VERSION = '2.3.3'
end
